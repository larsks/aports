# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Simon Zeni <simon@bl4ckb0ne.ca>
pkgname=spirv-tools
_pkgname=SPIRV-Tools
pkgver=1.3.224
_gitrev=7826e1941eab1aa66fbe84c48b95921bff402a96
pkgrel=0
pkgdesc="API and commands for processing SPIR-V modules"
url="https://github.com/KhronosGroup/SPIRV-Tools"
arch="all"
license="Apache-2.0"
depends_dev="spirv-headers $pkgname=$pkgver-r$pkgrel"
makedepends="$depends_dev cmake samurai python3"
subpackages="$pkgname-dev $pkgname-dbg"
source="$pkgname-$pkgver.tar.gz::https://github.com/KhronosGroup/SPIRV-tools/archive/$_gitrev.tar.gz"
builddir="$srcdir/$_pkgname-$_gitrev"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi

	# reduce size of debug syms
	CFLAGS="$CFLAGS -g1" CXXFLAGS="$CXXFLAGS -g1" \
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DBUILD_SHARED_LIBS=ON \
		-DSPIRV_WERROR=OFF \
		-DSPIRV-Headers_SOURCE_DIR=/usr \
		-DSPIRV_TOOLS_BUILD_STATIC=OFF \
		$CMAKE_CROSSOPTS
	cmake --build build
}

check() {
	# Not all test are enabled, because they rely on googletest source
	# folder. While it is easy to add, the (extra) tests then also need
	# effcee and re2 as well.
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
cd75c237b1dc3ea20d37f95a5e37d804a64c0eab40126ee417d5a0457afa76451143d1a4c9ece14a8b8bce7a7eae531de8c63ae5f3c2a19b371aa5a6f81ee01e  spirv-tools-1.3.224.tar.gz
"
