# Maintainer: psykose <alice@ayaya.dev>
pkgname=limine
pkgver=3.16.1
pkgrel=0
pkgdesc="Advanced multiprotocol x86/x86_64 BIOS/UEFI Bootloader"
url="https://limine-bootloader.org"
# only x86/x86_64 supported, x86 needs a cross toolchain
arch="x86_64"
license="BSD-2-Clause"
makedepends="
	clang
	coreutils
	mtools
	nasm
	"
subpackages="
	$pkgname-dev
	$pkgname-cd:_cd
	$pkgname-deploy
	$pkgname-pxe
	$pkgname-sys
	$pkgname-32:_32
	$pkgname-64:_64
	"
source="https://github.com/limine-bootloader/limine/releases/download/v$pkgver/limine-$pkgver.tar.xz"
options="!check" # no tests in tarball

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--enable-all
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

deploy() {
	pkgdesc="$pkgdesc (limine-deploy bios installer)"
	depends="$pkgname=$pkgver-r$pkgrel $pkgname-sys=$pkgver-r$pkgrel"

	amove usr/bin/limine-deploy
}

_cd() {
	pkgdesc="$pkgdesc (cd/efi files)"
	depends="$pkgname=$pkgver-r$pkgrel $pkgname-sys=$pkgver-r$pkgrel"

	amove usr/share/limine/limine-cd*.bin
}

pxe() {
	pkgdesc="$pkgdesc (pxe executable)"
	depends="$pkgname=$pkgver-r$pkgrel $pkgname-sys=$pkgver-r$pkgrel"

	amove usr/share/limine/limine-pxe.bin
}

sys() {
	pkgdesc="$pkgdesc (sys file)"
	depends="$pkgname=$pkgver-r$pkgrel"

	amove usr/share/limine/limine.sys
}

_32() {
	pkgdesc="$pkgdesc (32-bit uefi image)"
	depends="$pkgname=$pkgver-r$pkgrel"

	amove usr/share/limine/BOOTIA32.EFI
}

_64() {
	pkgdesc="$pkgdesc (64-bit uefi image)"
	depends="$pkgname=$pkgver-r$pkgrel"

	amove usr/share/limine/BOOTX64.EFI
}

sha512sums="
7a0d4df9417a7d7a83107ab543f27557b2c4d425f7a364a23058f0cf1eba6d7e5f9fc5274c8668eb834a4bb5c99b4f25fc2fdd90b34149c7c58d8c05f42483b6  limine-3.16.1.tar.xz
"
