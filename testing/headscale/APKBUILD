# Contributor: Patrycja Rosa <alpine@ptrcnull.me>
# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=headscale
pkgver=0.16.2
pkgrel=0
pkgdesc="An open source, self-hosted implementation of the Tailscale control server"
url="https://github.com/juanfont/headscale"
arch="all !ppc64le !riscv64" # go build fails
license="BSD-3-Clause"
pkgusers="headscale"
pkggroups="headscale"
makedepends="go"
subpackages="$pkgname-openrc"
install="$pkgname.pre-install"
source="https://github.com/juanfont/headscale/archive/v$pkgver/headscale-$pkgver.tar.gz
	headscale.initd
	"

export GOCACHE="$srcdir/go-cache"
export GOTMPDIR="$srcdir"
export GOMODCACHE="$srcdir/go"

prepare() {
	default_prepare

	# move socket to a subdirectory to allow running as non-root
	sed -i 's|/var/run/headscale.sock|/var/run/headscale/headscale.sock|' config-example.yaml
}

build() {
	make build
}

check() {
	make test
}

package() {
	install -Dm755 headscale "$pkgdir"/usr/bin/headscale

	install -Dm755 "$srcdir"/headscale.initd "$pkgdir"/etc/init.d/headscale
	install -Dm644 config-example.yaml "$pkgdir"/etc/headscale/config.yaml
}

sha512sums="
bad885866855211d43b45dcc7b958595967241af4c2a1b370532fef7c751b85002138cd3742e4046603af383c123b66afd7de9aba59e8eee600d7ffdd70ff12c  headscale-0.16.2.tar.gz
0800829bfc087af283afc117406324a0129b30b587c8cc5df85e147ac09fc879d726fc2d0b62ed545fb0190ed887641f07256745da9dea56932dd2d90aa41625  headscale.initd
"
