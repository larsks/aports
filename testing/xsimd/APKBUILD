# Contributor: Duncan Bellamy <dunk@denkimushi.com>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=xsimd
pkgver=8.1.0
pkgrel=0
pkgdesc="C++ wrappers for SIMD intrinsics and parallelized, optimized mathematical functions (SSE, AVX, NEON, AVX512)"
url="https://github.com/xtensor-stack/xsimd"
# only supports simd on these
arch="aarch64 x86 x86_64"
license="BSD-3-Clause"
makedepends="cmake gtest-dev linux-headers samurai"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/xtensor-stack/xsimd/archive/refs/tags/$pkgver.tar.gz
	failed-tests.patch
	"
builddir="$srcdir/"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DBUILD_TESTS="$(want_check && echo ON || echo OFF)" \
		-S $pkgname-$pkgver \
		$CMAKE_CROSSOPTS
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
539f7b565b45e8225c6476ca1becc8243a84ae7fb51b45a584231e7d36aee10a09d7d30fb87d89cb77813fb063a7b7617bcf01fdf996f59d99e8d474d2a044ee  xsimd-8.1.0.tar.gz
45cf1c6cdf9db3eb840239b93906c50fc551eabd487ae40e1e050680c836aa559801aaee02c45e32023b5c35945f70113f82f1d0f6945ae03875b78fe94a0147  failed-tests.patch
"
